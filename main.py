def straight_triangle(side_length, fill: str = '*'):
    triangle = ""

    for line in range(side_length):
        triangle += f"{fill * (line + 1)}\n"

    return triangle


if __name__ == "__main__":
    user_side_length = int(input("Enter a number: "))
    print(straight_triangle(user_side_length, fill='1'))
